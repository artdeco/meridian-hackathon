#!/usr/bin/env node
             
const fs = require('fs');             
/*
 [@artdeco/]erte: String difference with colour and CLI ANSI formatting.

 Copyright (C) 2020  Art Deco

 This program is NOT free software: you cannot redistribute it and/or
 modify, reverse-engineer and embed into other programs under the terms
 of the Art Deco EULA which comes together with this distribution, and
 is available at artdeco.legal. You're allowed to use this program as
 a LIB with the license key issued per user.

*/
/*
 diff package https://github.com/kpdecker/jsdiff
 BSD License
 Copyright (c) 2009-2015, Kevin Decker <kpdecker@gmail.com>
*/
const u = {black:30, red:31, green:32, yellow:33, blue:34, magenta:35, cyan:36, white:37, grey:90}, v = {reset:0, bold:1, underline:4, j:5, i:6, reverse:7, conceal:8, g:9, h:10}, w = a => {
  Array.isArray(a) && (a = a.join(";"));
  return `\x1b[${a}m`;
};
function x(a) {
  const b = [u.yellow, ...Object.keys({}).map(d => v[d.toLowerCase()])].filter(Boolean);
  return b.length ? `${w(b)}${a}${w(v.reset)}` : a;
}
;const y = (a, b, d, c = !1, f = !1) => {
  const g = d ? new RegExp(`^-(${d}|-${b})$`) : new RegExp(`^--${b}$`);
  b = a.findIndex(e => g.test(e));
  if (-1 == b) {
    return {argv:a};
  }
  if (c) {
    return {value:!0, index:b, length:1};
  }
  c = a[b + 1];
  if (!c || "string" == typeof c && c.startsWith("--")) {
    return {argv:a};
  }
  f && (c = parseInt(c, 10));
  return {value:c, index:b, length:2};
}, z = a => {
  const b = [];
  for (let d = 0; d < a.length; d++) {
    const c = a[d];
    if (c.startsWith("-")) {
      break;
    }
    b.push(c);
  }
  return b;
}, B = () => {
  var a = A;
  return Object.keys(a).reduce((b, d) => {
    const c = a[d];
    if ("string" == typeof c) {
      return b[`-${c}`] = "", b;
    }
    d = c.command ? d : `--${d}`;
    c.short && (d = `${d}, -${c.short}`);
    let f = c.description;
    c.default && (f = `${f}\nDefault: ${c.default}.`);
    b[d] = f;
    return b;
  }, {});
};
const C = fs.readFileSync, D = fs.writeFileSync;
function E() {
  var a = {description:"The Implementation Of Package.Market For The Meridian Hackathon Submission.", example:"meridian-hackathon example.txt -o out.txt", line:"meridian-hackathon input [-o output] [-ihv]", usage:B()};
  const {usage:b = {}, description:d, line:c, example:f} = a;
  a = Object.keys(b);
  const g = Object.values(b), [e] = a.reduce(([h = 0, k = 0], m) => {
    const p = b[m].split("\n").reduce((q, r) => r.length > q ? r.length : q, 0);
    p > k && (k = p);
    m.length > h && (h = m.length);
    return [h, k];
  }, []), n = (h, k) => {
    k = " ".repeat(k - h.length);
    return `${h}${k}`;
  };
  a = a.reduce((h, k, m) => {
    m = g[m].split("\n");
    k = n(k, e);
    const [p, ...q] = m;
    k = `${k}\t${p}`;
    const r = n("", e);
    m = q.map(t => `${r}\t${t}`);
    return [...h, k, ...m];
  }, []).map(h => `\t${h}`);
  const l = [d, `  ${c || ""}`].filter(h => h ? h.trim() : h).join("\n\n");
  a = `${l ? `${l}\n` : ""}
${a.join("\n")}
`;
  return f ? `${a}
  Example:

    ${f}
` : a;
}
;async function F(a = {}) {
  const {shouldRun:b = !0, text:d = ""} = a;
  if (!b) {
    return "";
  }
  console.log("@luddites/meridian-hackathon called with %s", x(d));
  return d;
}
;const I = (a, b) => {
  a.prototype.a = b.reduce((d, {name:c, prototype:{a:f = []} = {}}) => {
    d.push(c, ...f);
    return d;
  }, [a.name]).filter(Boolean).filter((d, c, f) => f.indexOf(d) == c);
}, J = a => Object.entries(a).reduce((b, [d, c]) => {
  if (void 0 === c) {
    return b;
  }
  b[d] = c;
  return b;
}, {}), K = a => {
  var b = {};
  a = Object.getOwnPropertyDescriptors(a);
  Object.entries(a).forEach(([d, c]) => {
    const f = b[d];
    c = J(c);
    return b[d] = {...f, ...c};
  }, {});
  return b;
}, L = (a, b, d) => {
  if (a && a.constructor !== Object && (a = a.__proto__)) {
    var c = (Object.getOwnPropertyDescriptor(a, b) || {})[d];
    return c ? c : L(a, b, d);
  }
}, N = a => {
  const b = {};
  [...M(a.prototype), a.prototype].forEach(d => {
    d = K(d);
    Object.defineProperties(b, d);
  });
  return b;
}, M = (a, b = []) => {
  if (!a) {
    return b;
  }
  a = a.__proto__;
  if (!a || a.constructor === Object) {
    return b;
  }
  b.unshift(a);
  return M(a, b);
};
class O {
  constructor() {
    this.example = "ok";
  }
  async run() {
  }
}
;class P extends O {
  constructor() {
    super();
  }
}
(function(a, b, ...d) {
  var c = d.reduce((g, e) => {
    if ("function" == typeof e) {
      return g;
    }
    Object.setPrototypeOf(e, a.prototype);
    e = K(e);
    Object.defineProperties(g, e);
    return g;
  }, {}), f = d.map(g => "function" != typeof g ? null : N(g)).filter(Boolean);
  f.length && (f = [a.prototype, ...f].reduce((g, e) => {
    e = K(e);
    Object.defineProperties(g, e);
    return g;
  }, {}), f = Object.getOwnPropertyDescriptors(f), Object.defineProperties(a.prototype, f));
  c = Object.getOwnPropertyDescriptors(c);
  Object.entries(c).forEach(([g, e]) => {
    const n = e.value;
    var l;
    let h, k;
    if ("function" == typeof n) {
      const m = (Object.getOwnPropertyDescriptor(a.prototype, g) || {}).value;
      "function" == typeof m && (k = function(...p) {
        m.call(this, ...p);
        return n.call(this, ...p);
      });
    }
    e.get && !e.set ? l = L(b.prototype, g, "set") : e.set && !e.get && (h = L(b.prototype, g, "get"));
    l = J({value:k, set:l, get:h});
    Object.defineProperty(b.prototype, g, {...e, ...l});
  });
  I(b, d);
})(O, P, ...[function() {
}.prototype = {async run(a) {
  return await F(a);
}}]);
async function Q(a) {
  const {interval:b = 250, writable:d = process.stdout} = {};
  a = "function" == typeof a ? a() : a;
  const c = d.write.bind(d);
  var f = process.env.INDICATRIX_PLACEHOLDER;
  if (f && "0" != f) {
    return c("Initialising the package<INDICATRIX_PLACEHOLDER>"), await a;
  }
  let g = 1, e = `${"Initialising the package"}${".".repeat(g)}`;
  c(e);
  f = setInterval(() => {
    g = (g + 1) % 4;
    e = `${"Initialising the package"}${".".repeat(g)}`;
    c(`\r${" ".repeat(27)}\r`);
    c(e);
  }, b);
  try {
    return await a;
  } finally {
    clearInterval(f), c(`\r${" ".repeat(27)}\r`);
  }
}
;async function R() {
  const a = new Promise(b => setTimeout(b, 2000));
  await Q(a);
}
;const A = {input:{description:"The path to the input file.", command:!0}, output:{description:"Where to save the output. By default prints to stdout.", default:"-", short:"o"}, init:{description:"Initialise in the current folder.", boolean:!0, short:"i"}, help:{description:"Print the help information and exit.", boolean:!0, short:"h"}, version:{description:"Show the version's number and exit.", boolean:!0, short:"v"}}, S = function(a = {}, b = process.argv) {
  let [, , ...d] = b;
  const c = z(d);
  d = d.slice(c.length);
  a = Object.entries(a).reduce((e, [n, l]) => {
    e[n] = "string" == typeof l ? {short:l} : l;
    return e;
  }, {});
  const f = [];
  a = Object.entries(a).reduce((e, [n, l]) => {
    let h;
    try {
      const k = l.short, m = l.boolean, p = l.number, q = l.command, r = l.multiple;
      if (q && r && c.length) {
        h = c;
      } else {
        if (q && c.length) {
          h = c[0];
        } else {
          const t = y(d, n, k, m, p);
          ({value:h} = t);
          const G = t.index, H = t.length;
          void 0 !== G && H && f.push({index:G, length:H});
        }
      }
    } catch (k) {
      return e;
    }
    return void 0 === h ? e : {...e, [n]:h};
  }, {});
  let g = d;
  f.forEach(({index:e, length:n}) => {
    Array.from({length:n}).forEach((l, h) => {
      g[e + h] = null;
    });
  });
  g = g.filter(e => null !== e);
  Object.assign(a, {f:g});
  return a;
}(A), T = S.input, U = S.output || "-", V = S.init, W = S.version;
if (S.help) {
  const a = E();
  console.log(a);
  process.exit(0);
} else {
  W && (console.log(require("../../package.json").version), process.exit(0));
}
(async() => {
  try {
    if (V) {
      return await R();
    }
    if (!T) {
      throw Error("Please pass an input file.");
    }
    const a = C(T, "utf8"), b = await F({shouldRun:!0, text:a});
    "-" == U ? console.log(b) : D(U, b);
    console.error("File %s successfully processed.", x(T));
  } catch (a) {
    process.env.DEBUG ? console.error(a.stack) : console.log(a.message);
  }
})();


//# sourceMappingURL=meridian-hackathon.js.map