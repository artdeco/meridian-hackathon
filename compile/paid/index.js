const { _meridianHackathon, _MeridianHackathon } = require('./compile/meridian-hackathon')

/**
 * The Implementation Of Package.Market For The Meridian Hackathon Submission.
 * @param {!MeridianHackathonConfig} config Additional options for the program.
 * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
 * - `[text]` _string?_ A text to return.
 * @return {!Promise}
 */
function $meridianHackathon(config) {
  return _meridianHackathon(config)
}

/**
 * A representation of a package.
 * @typedef {MeridianHackathon}
 */
class MeridianHackathon extends _MeridianHackathon {
  /**
   * Constructor method.
   */
  constructor() {
    super()
    /**
     * An example property. Default `ok`.
     * @type {string}
     */
    this.example
  }
  /**
   * Executes main method via the class.
   * @param {!MeridianHackathonConfig} conf The config to run the method against.
   * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
   * - `[text]` _string?_ A text to return.
   * @return {!Promise<string>} The result of transforms.
   * @example
   * ```js
   * import { MeridianHackathon } from '@luddites/meridian-hackathon'
   *
   * let meridian-hackathon = new MeridianHackathon()
   * let res = await meridian-hackathon.run({
   *   shouldRun: true,
   *   text: 'hello-world',
   * })
   * console.log(res) // @luddites/meridian-hackathon called with hello-world
   * ```
   */
  async run(conf) {
    return super.run(conf)
  }
}

module.exports = exports = $meridianHackathon
module.exports.MeridianHackathon = MeridianHackathon

/** @typedef {$meridianHackathon} meridianHackathon */

/* typal types */
/**
 * @typedef {Object} MeridianHackathonConfig `＠record` Additional options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */
