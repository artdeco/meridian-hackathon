<!-- ## TODO

- [ ] Add a new item to the todo list. -->

## Copyright & License

See `@artdeco/EULA` in [LICENSE](LICENSE). **Not** Open Source.
You'll be sued when using any of the source code without permission.

<footer />

%~ -1%