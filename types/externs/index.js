/**
 * @fileoverview
 * @externs
 */

/* typal types */
/** @const */
var _meridianHackathon = {}
/**
 * Additional options for the program.
 * @record
 */
_meridianHackathon.MeridianHackathonConfig = function() {}
/**
 * A boolean option. Default `true`.
 * @type {boolean|undefined}
 */
_meridianHackathon.MeridianHackathonConfig.prototype.shouldRun
/**
 * A text to return.
 * @type {string|undefined}
 */
_meridianHackathon.MeridianHackathonConfig.prototype.text
/**
 * The Implementation Of Package.Market For The Meridian Hackathon Submission.
 * @typedef {function(!_meridianHackathon.MeridianHackathonConfig): !Promise}
 */
_meridianHackathon.meridianHackathon
/**
 * A representation of a package.
 * Constructor method.
 * @interface
 */
_meridianHackathon.MeridianHackathon = function() {}
/**
 * An example property. Default `ok`.
 * @type {string}
 */
_meridianHackathon.MeridianHackathon.prototype.example
/**
 * Executes main method via the class.
 * @param {!_meridianHackathon.MeridianHackathonConfig} conf The config to run the method against.
 * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
 * - `[text]` _string?_ A text to return.
 * @return {!Promise<string>} The result of transforms.
 * @example
 * ```js
 * import { MeridianHackathon } from '@luddites/meridian-hackathon'
 *
 * let meridian-hackathon = new MeridianHackathon()
 * let res = await meridian-hackathon.run({
 *   shouldRun: true,
 *   text: 'hello-world',
 * })
 * console.log(res) // @luddites/meridian-hackathon called with hello-world
 * ```
 */
_meridianHackathon.MeridianHackathon.prototype.run = function(conf) {}
