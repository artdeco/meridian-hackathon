const run = require('./run');
const AbstractMeridianHackathon = require('../../types/lux');

class MeridianHackathon extends AbstractMeridianHackathon {
  constructor() {
    super()
  }
}

/** @constructor @extends {MeridianHackathon} @suppress {checkTypes} */ function _MeridianHackathon() {}
AbstractMeridianHackathon.__implement(MeridianHackathon, _MeridianHackathon.prototype = /** @type {!MeridianHackathon} */ ({
  async run(conf) {
    return await run(conf)
  },
}))

module.exports = MeridianHackathon