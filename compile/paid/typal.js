import { _meridianHackathon, _MeridianHackathon } from './compile/meridian-hackathon'

/** @api {_meridianHackathon.meridianHackathon} */
export default { _meridianHackathon }

/** @api {_meridianHackathon.MeridianHackathon} */
export { _MeridianHackathon }

/* typal types */
