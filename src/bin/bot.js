import { c } from '@artdeco/erte'
import envariable from 'envariable'
import Bot from 'keybase-bot'
import { aqt } from 'rqt'
import { dirname, join } from 'path'
import { spawnKeybase } from '../lib/spawn-keybase'
import { claim, createClaimableBalance, getBalances, transfer } from '../lib/snippet'
import { Keypair } from 'stellar-sdk'

if (process.env != 'production') {
  envariable(); envariable({
    name: '.settings',
  })
}
const {
  KB_PAPERKEY, KB_USERNAME, HOST = `https://packages.ludds.io`,
  STELLAR_SECRET,
} = process.env

let t = 0

const BOT_NAME = KB_USERNAME

/** @type {Bot} */
let bot = null
async function start() {
  bot = new Bot()
  t = Date.now()
  await bot.init(KB_USERNAME, KB_PAPERKEY)
  // await bot.initFromRunningService('', {
  //   botLite: false,
  // })
  t = Date.now() - t
  console.log('Init bot in %sms', t)
  const info = bot.myInfo()
  console.log(info)

  // const status = await spawnKeybase(info, 'status')
  // console.log(status.stdout)
  try {
    const res = await spawnKeybase(info, 'fs', 'ls', `keybase://private/${info.username}`)
    console.log(res.stdout)
  } catch (err) {
    // hm
  }

  console.log('bot init ok')
  await bot.chat.clearCommands()
  await bot.chat.advertiseCommands({
    advertisements: [
      {
        type: 'public',
        commands: [
          {
            name: 'echo',
            description: 'Sends out your message to the current channel.',
            usage: '[your text]',
          },
          {
            name: 'publish',
            description: 'Scans for new versions in your keybase://public/packages and adds them to the registry.',
            usage: '@scope/package-name 1.1.1',
          },
          {
            name: 'private-versions',
            description: 'Shows the information about the package in the private folder.',
            usage: '@scope/package-name',
          },
          {
            name: 'purchase',
            description: 'Adds the package to your purchases, allowing you to download it.',
            usage: '@scope/package-name',
          },
          {
            name: 'purchases',
            description: 'View the list of your purchases.',
          },
          {
            name: 'balance',
            description: 'Check your current balance in Ludds that can be spent on packages.',
            // usage: '@scope/package-name',
          },
          {
            name: 'topup',
            description: 'Generates a payment link for you to buy Ludds to spend on software.',
            usage: 'amount, e.g., 100',
          },
          {
            name: 'account',
            description: 'Information about your account. Use `reset` to forget the test account (be careful!).',
            usage: '[reset]',
          },
          {
            name: 'claim',
            description: 'A debug method to claim the balance that was created for the account.',
            // usage: 'secret-key',
          },
        ],
      },
    ],
  })
  bot.chat.watchAllChannelsForNewMessages(async (message) => {
    const reply = async (body) => {
      await bot.chat.send(message.conversationId, {
        body,
      })
    }
    const error = async (body) => {
      await bot.chat.send(message.conversationId, {
        body: `*ERROR*: ${body}`,
      })
    }
    console.log(message)
    const { sender: { username } } = message

    if (message.content.type != 'text') {
      return
    }

    const [cmd, ...rest] = message.content.text.body.split(/\s+/)

    try {
      switch (cmd) {
      case '!account': {
        const [reset] = rest
        const { entryValue } = await bot.kvstore.get('', username, 'account')
        if (!entryValue) {
          return reply('You don\'t have an account. Use the !topup method to add funds and automatically create a new account.')
          // please pay 10$ for us to top up
        }
        if (reset) {
          await bot.kvstore.delete('', username, 'account')
          await bot.kvstore.delete('', username, `packages`)
          return reply('Successfully destroyed an account. Use !topup to create a new one.')
        }
        return reply(`Your secret key is: \`${entryValue}\`. Please keep it safe. Claim any balances with the !claim command and use !balance to check remaining credit.`)
      }
      case '!claim': {
        const { entryValue: secret } = await bot.kvstore.get('', username, 'account')
        if (!secret) throw new Error('You don\'t have an account yet. Please check !account method first.')
        await claim(secret)
        const publicKey = Keypair.fromSecret(secret).publicKey()
        const s = await getBalances(publicKey)
        await reply('Successfully claimed the balance.\n'+ s)
        break
      }
      case '!balance': {
        const { entryValue: secret } = await bot.kvstore.get('', username, 'account')
        const publicKey = Keypair.fromSecret(secret).publicKey()
        const s = await getBalances(publicKey)
        await reply('Use credits to purchase packages. Your balances are:\n'+ s)
        break
      }
      case '!topup': {
        const { entryValue } = await bot.kvstore.get('', username, 'account')
        const XLM = 100
        if (!entryValue) {
          await reply(`You've paid 10$. Going to top up ${XLM} XLM.`)
          const pair = await createClaimableBalance(100, STELLAR_SECRET)
          const secret = pair.secret()
          await bot.kvstore.put('', username, 'account', secret)
        } else {
          const publicKey = Keypair.fromSecret(entryValue).publicKey()
          await createClaimableBalance(100, STELLAR_SECRET, publicKey)
        }
        await reply(`Operation was successful. Use !claim to claim your new balance of ${XLM} XLM.`)
        break
      }
      case '!purchases': {
        let { entryValue } = await bot.kvstore.get('', username, `packages`)
        if (!entryValue) {
          return await reply('You haven\'t purchased any packages.')
        }
        const packages = JSON.parse(entryValue) || {}
        return await reply(`You've purchased:` + '\n```' + Object.keys(packages).join('\n') + '\n```')
      }
      case '!purchase': {
        const [pckg] = rest
        if (!pckg) throw new Error('Please specify the package name.')

        const { entryValue } = await bot.kvstore.get('', username, `packages`)
        const packages = JSON.parse(entryValue) || {}
        if (packages[pckg]) {
          return await reply(`You've already purchase the package.`)
        }
        const { entryValue: secret } = await bot.kvstore.get('', username, `account`)
        if (!secret) {
          return reply('You don\'t have an account. Use the !topup method to add funds and automatically create a new account.')
        }
        const publicKey = Keypair.fromSecret(STELLAR_SECRET).publicKey()
        const PRICE = 10
        await transfer(secret, publicKey, PRICE)
        packages[pckg] = { price: PRICE, date: new Date() }
        await bot.kvstore.put('', username, `packages`, JSON.stringify(packages))

        // let { entryValue: purchases = '' } = await bot.kvstore.get('', username, `purchases`)
        // purchases = purchases + '\n' + pckg + ' - ' + PRICE + 'XLM'
        // await bot.kvstore.put('', username, `purchases`, purchases)

        return reply(`You've successfully purchased a new package *${pckg}* for ${PRICE} XLM. Please update your .npmrc to include the registry for its scope.`)
      }
      case '!private-versions': {
        const [pckg] = rest
        if (!pckg) throw new Error('Please specify the package name.')
        const { versions } = await getPublic(username, pckg)
        return await reply(Object.keys(versions).map(a => `- ${a}`).join('\n'))
      }
      case '!publish': {
        const [pckg, version] = rest
        if (!pckg) throw new Error('Please specify the package name.')
        if (!version) throw new Error('Please specify the version.')
        const { versions: { [version]: v } } = await getPublic(username, pckg)
        if (!v) throw new Error(`Version ${version} of package ${pckg} not found.`)

        const { dist: { tarball } } = v
        // https://ludds.io/nedludd/depack/-/depack-1.1.2-free.tgz
        const file = tarball.replace(`${HOST}/${username}/`, '')
        const p = join('private', `${BOT_NAME},${username}`, 'packages', file)
        const PRIVATE_TAR = `keybase://${p}` // check if it exists

        await spawnKeybase(info, 'fs', 'ls', PRIVATE_TAR) // make sure the package is there, throws otherwise
        const PATH = `packages/${username}/${file}`
        const KBPBOT_DIR = `keybase://private/${BOT_NAME},kbpbot`
        await ensureKeybasePath(info, KBPBOT_DIR, PATH)
        const PUBLIC_PATH = [KBPBOT_DIR, PATH].join('/')
        await spawnKeybase(info, 'fs', 'cp', PRIVATE_TAR, PUBLIC_PATH)
        await reply(`Successfully published ${pckg} version ${version}.`)
        break
      }
      case '!echo': {
        await bot.chat.send(message.conversationId, {
          body: message.content.text.body.substr(6),
        })
      }
      }
    } catch (err) {
      logError(err)
      await error(err.message)
    }
  }, (err) => {
    console.log(c('An error when listening for messages', 'yellow'))
    logError(err)
  })
  return bot
}

(async () => {
  try {
    bot = await start()
  } catch (err) {
    logError(err)
  }
})()

async function ensureKeybasePath(info, root, path) {
  const dir = dirname(path)
  let n = ''
  const d = dir.split('/')
  for (const dd of d) {
    n = [n, dd].join('/')
    const j = [root, n].join('/')
    await spawnKeybase(info, 'fs', 'mkdir', j)
  }
}

async function shutDown() {
  try {
    await bot.deinit()
  } finally {
    process.exit()
  }
}

process.on('SIGINT', shutDown)
process.on('SIGTERM', shutDown)

async function getPublic(username, pckg) {
  const url = `https://${username}.keybase.pub/packages/${pckg}`
  const { body, statusCode } = await aqt(url)
  if (statusCode != 200) throw new Error(`Package ${pckg} not found.`)
  return JSON.parse(body)
}

/**
 *
 * @param {Error} err
 */
function logError(err) {
  const s = err.stack.replace(err.message, '')
  console.log(c(err.message, 'red'))
  console.log(c(s, 'grey'))
}