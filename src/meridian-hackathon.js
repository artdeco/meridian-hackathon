import '../types/externs'
import meridianHackathon, { MeridianHackathon } from './'

module.exports = {
  '_meridianHackathon': meridianHackathon,
  '_MeridianHackathon': MeridianHackathon,
}