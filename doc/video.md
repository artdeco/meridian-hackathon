JavaScript is the language of the web. Traditionally, all packages are
placed on the main registry known as NPM. However, the package management
clients allow for any number of registries, for which .npmrc is used,
where each scope is given its own internet address. The access to the
registry is controlled via authentication, such as a bearer token or
basic http authorisation. Unfortunately, basic auth has been removed from
npm, but is still present in yarn, which allows to set up access control
to packages easily.

This project exploits the public infrastructure constructed by Keybase
that revolves across Keybase pages. Users will be able to create their
own registries, and host indecies on their own subdomain at keybase.pub
so that package consumers know for sure that the publishes is verified.
The downloads link found on those indecies will point to packages archives
which are also served by Keybase pages, but are private between the chat
bot, through which the packages are now published, and the Keybase. Users
can talk to the chat bot to purchase a package, after which their username
is added to the access control list that allows to read the distributed
archives from Keybase.

To build their registries, producers use a CLI tool
that maintains a local database their packages.
Instead of calling `npm publish` they'll now use `neoluddite` to pack their software
and add it to the registry by generating indicies and placing the packed archives in a
private folder shared with a bot named `ludd`. They then talk to
`ludd` to publish new versions, and he will copy tar achieves into its
own private folder, shared with Keybase Pages Bot making it available on the web via _packages.ludds.io_ domain. By modifying the `.kbp_conf` file, the bot can grant access only only to those people who've paid for the packages.

This was the publisher journey. The consumer journey involves people who'd like
to purchase packages talking to _Ludd_, also. Those who are new to the cryptocurrency,
can seamlessly get onboarded, as _Ludds.io_ will create an accounts for them in no time.
and sponsor its resevers. Once the offline transaction is confirmed, the newly
created account is issued a claimable balance against it, so that the new user can
claim funds in their own time. This reduces the friction and prevents abandoning
of purchases.

In the demo, we create an account for new users, and store its secret key in the Keybase
datastore, so that they can claim balances through us.
But once the Stellar infrastructure upgrades to the new protocol and wallets
support claiming balances, this step will be removed. As packages might have some
dependencies which also need to be paid for, the total price will be accumulated
and payment distributed accordingly.
Ideally, Ludds.io hopes to grow into an anchor that facilitates sustainable software
economy by charging small transaction fees while allowing each developer to get paid fairly.