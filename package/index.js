import { format } from 'url'
import { dependencies } from '../package'
import { website } from './common'

export let name = '@luddites/meridian-hackathon'
export let description = 'The Implementation Of Package.Market For The Meridian Hackathon Submission.'

export let keywords = 'meridian, stellar'
export let author = 'Anton <anton@adc.sh>'
export let bugs = {
  url: 'https://gitlab.com/luddites/meridian-hackathon/-/issues',
  email: 'incoming+luddites-meridian-hackathon-22313206-issue-@incoming.gitlab.com'
}
export let homepage = format(website)

const SEE_LICENSE = 'SEE LICENSE IN EULA.md'
const EULA = 'node_modules/@artdeco/license/EULA.md'
const CHANGELOG = 'CHANGELOG.md'
const README = 'README.md'
const ECR = [EULA, CHANGELOG, README]

export const NPM = {
  license: SEE_LICENSE,
  addFiles: [...ECR,
    'compile/paid/index.js'],
  bin: {
    'meridian-hackathon': 'compile/bin/meridian-hackathon.js',
  },
  typedefs: 'typedefs.json',
}

// // work in progress
// export const LIB = {
//   module: 'src/index.js',
//   externs: 'types/externs.js',
//   addFiles: [
//     'package/EULA', 'src',
//     'types', 'example/jsdoc/',
// 
//     ...compile,
// 
// 
//   ],
//   license: 'SEE LICENSE IN EULA',
// 
//   bin: { meridian-hackathon: 'bin/meridian-hackathon.js' },
// 
// }


export const LOCAL_COMPILE = {
  version: `local-${process.env.VERSION}-compile`,
  bin: {
    'meridian-hackathon': 'compile/bin/meridian-hackathon.js',
  },
}