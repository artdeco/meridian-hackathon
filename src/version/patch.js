import { readFileSync, writeFileSync } from 'fs'
import { join } from 'path'
import { c } from '@artdeco/erte'

const [,,version] = process.argv

const PATH = join(__dirname, 'index.js')
let f = readFileSync(PATH, 'utf-8')

const isFree = version == '--free'

if (isFree) {
  f = f.replace("\nimport isFree from './paid'", "\n// import isFree from './paid'")
  f = f.replace("\n// import isFree from './free'", "\nimport isFree from './free'")
  console.log('Set version to %s', c('free', 'red'))
} else {
  f = f.replace("\n// import isFree from './paid'", "\nimport isFree from './paid'")
  f = f.replace("\nimport isFree from './free'", "\n// import isFree from './free'")
  console.log('Set version to %s', c('paid', 'magenta'))
}

writeFileSync(PATH, f)