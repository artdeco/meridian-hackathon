const { c } = require('../../compile/stdlib');

/**
 * @type {_meridianHackathon.meridianHackathon}
 */
async function meridianHackathon(config = {}) {
  const {
    shouldRun = true,
    text = '',
  } = config
  if (!shouldRun) return ''
  console.log('@luddites/meridian-hackathon called with %s', c(text, 'yellow'))
  return text
}

/** @typedef {import('../../').meridianHackathon} _meridianHackathon.meridianHackathon */


module.exports = meridianHackathon