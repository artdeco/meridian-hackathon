// use .alamoderc to rewrite to `./paid`
import isFree from './free'
// import isFree from './paid'

export class PaidError extends Error {
  /**
   * @param {string} feature
   */
  constructor(feature) {
    super(`${feature} is only supported in the paid version.`)
    if (!isFree) throw new Error('The paid version shouldn\'t create paid errors.')
  }
  get code() {
    return 'NOTPAID'
  }
}
/**
 * Whether the library is being compiled for the free version.
 * @type {boolean}
 */
export const free = isFree
// export const free = true