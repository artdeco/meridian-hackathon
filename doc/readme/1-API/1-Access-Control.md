## Access Control

The access to the registry can be controlled with [`.kbp_config` file](https://book.keybase.io/sites#more-customizations)
where the password is hashed with bcrypt encoded string (`username -> bcrypt-hashed password`)

- Docs on https://pkg.go.dev/github.com/keybase/client@v5.3.1+incompatible/go/kbfs/libpages/config

%EXAMPLE: .kbp_config, json%

When the password is *hello*, it needs to be set as **aGVsbG8=** in `.npmrc`:

```
# .npmrc
https://packages.ludds.io/neddludd:_password=aGVsbG8=
https://packages.ludds.io/neddludd:username=user
```

%~%
