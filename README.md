<div align="center"><p align="center">

# @luddites/meridian-hackathon

[![npm version](https://badge.fury.io/js/%40luddites%2Fmeridian-hackathon.svg)](https://www.npmjs.com/package/@luddites/meridian-hackathon)
<pipeline-badge/>
</p></div>

`@luddites/meridian-hackathon` is: The Implementation Of Package.Market For The Meridian Hackathon Submission.

```sh
yarn add @luddites/meridian-hackathon
npm i @luddites/meridian-hackathon
```

## Table Of Contents

- [Table Of Contents](#table-of-contents)
- [Submission](#submission)
  * [Inspiration](#inspiration)
  * [What it does](#what-it-does)
  * [How I built it](#how-i-built-it)
  * [Challenges I ran into](#challenges-i-ran-into)
  * [Accomplishments that I'm proud of](#accomplishments-that-im-proud-of)
  * [What I learned](#what-i-learned)
  * [What's next for Łudds: Package Currency. Encouraging Paid Software Business.](#whats-next-for-łudds-package-currency-encouraging-paid-software-business)
- [Access Control](#access-control)
- [CLI](#cli)
- [Copyright & License](#copyright--license)

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></p></div>

- Ludds.io: Package Market. Encouraging Paid Software.
- All works of art deserve to be paid and software is no different. Let's rethink current approach to package distribution and start charging people for our hard work on the new package.market.

## Submission

This is the text for the Hackathon Page.

### Inspiration

I really tried to identify myself with the world of Open Source, however couldn't in the end and gave up. Having made over 200 npm packages, including a novel testing and documentation frameworks, I've discovered that people are simply not interested in my work. It seems that once cool, open-minded movement of techies has become a biased, entitled mass that thinks "software = free" and that's everything there is to it. After finally realising that nobody is going to support my work, I decided to do what's usually done to make a living: sell my software.

In Democracy, people are allowed to hold various informed opinions. I'd like to give people the choice of selling their packages. There are many, who wouldn't mind putting a price tag on a piece of code, which they thoroughly tested, documented and perfected during sleepless hours, but the industry doesn't give them a chance simply because there's no platform. When estimated, many would find themselves surprised that software that they upload online for free, costs at least a few thousand dollars, and can bring recurring revenue.

Open Source isn't really fair to everyone. Once a maintainer has captured the market with his free tool, he/she will dominate it simply because the tool becomes the standard, and nobody else receives a chance to showcase their great packages and build up the market share, too. The "free" model evolves into predatory pricing strategy, while it is the customer who's suffering when they cannot receive adequate customer service because the maintainers use excuses like "we're volunteers" even though they're not providing fair support plans either.

I don't want donations. I don't want community. I want to build, market and sell. I want to create a brand and earn reputation as a software vendor whom individual customers as well as companies love dealing with, because they know that I care. My view is completely antagonistic to the FOSS paradigm, and I believe that although the user might feel like she has the right to study and improve the program, the software publisher also has the right to keep the source closed as his intellectual property. That's why I'm passionate about building a package market.

### What it does

The entry point to the app is two-fold.

First, package makers upload their work to the new registry (ludds.io). Using Keybase as the cryptographically sound client behind the package storage, the registry allows package makers to:

- upload the indexes of packages (such as @scope/package-name) to their public folder at username.keybase.pub/packages so that other people are able to access those indexes and be sure that they were created by the publisher, and not tampered with. The download links to actual archives with distributed material are signed using npm's and yarn's shasum algorithm to ensure authenticity.
- upload the actual packages to a private folder at `keybase://private/username,ludd` which is shared with the bot called ludd. Once uploaded, the bot moves the tar.gz archives onto its own private folder shared with kbpbot that will serve them from Keybase pages instance. The packages.ludds.io domain is configured to serve the files according to Keybase's documentation.

The security of packages is ensured by the fact that the index file is downloadable from the user's pub directory, visually identifiable as USERNAME.keybase.pub. As shasums are present, even though the domain that hosts the archives isn't transparent, the consumers are sure that the package was published by the real owner.

Secondly, those who wish to consume packages, need talk to the `ludd` chat bot, who'll process their requests. Users can search for packages using keywords and natural language (not implemented in hackathon) and once they found the suitable package, complete the purchase.

### How I built it

### Challenges I ran into

### Accomplishments that I'm proud of

### What I learned

### What's next for Łudds: Package Currency. Encouraging Paid Software Business.

OK

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="/.documentary/section-breaks/1.svg?sanitize=true">
</a></p></div>

## Access Control

The access to the registry can be controlled with [`.kbp_config` file](https://book.keybase.io/sites#more-customizations)
where the password is hashed with bcrypt encoded string (`username -> bcrypt-hashed password`)

- Docs on https://pkg.go.dev/github.com/keybase/client@v5.3.1+incompatible/go/kbfs/libpages/config

```json
{
  "version": "v1",
  "users": {
    "user": "$2y$12$G9WEY55xbcxDp40xzTeQReT661MAI5qWUoKvsyQFkbEw5O2B5SEdW"
  },
  "per_path_configs": {
    "/": {
      "whitelist_additional_permissions": {
        "user": "read"
      },
      "anonymous_permissions": "list"
    }
  }
}
```

When the password is *hello*, it needs to be set as **aGVsbG8=** in `.npmrc`:

```
# .npmrc
https://packages.ludds.io/neddludd:_password=aGVsbG8=
https://packages.ludds.io/neddludd:username=user
```

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="/.documentary/section-breaks/2.svg?sanitize=true">
</a></p></div>


## CLI

There are 2 CLI programs that are used.

First, `neoluddite` is a piece of software that builds registry indexes from the
local database that contains meta records about packages. This is outside
the scope of this hackathon and was developed as part of the _Luddites_ projects.

However, it was modified to adapt for the needs of the hackathon in the following ways:

1. The `blank` argument was added to enable to generate indexes without extension.
GitLab, for example, could serve `index.html` at `https://registry.gitlab.com/@scope/package/`
appropriately, but Keybase will serve a redirect request to `package/` which won't be
handled by `npm` and `yarn` clients appropriately. This means that we can simply create
`@scope/package` file with the JSON information about the versions that are then installed.
1. An additional `tars` option was added, to specify a distinct folder where the packed
packages are placed. Previously, they would go in the public folder, but because the
the indexes are now files and not `index.html` files inside directories, TARs need to go
into a separate folder, as one cannot write to `@scope/package`.
1. Finally, in the previous version, the registry was maintained as a single repository
where indexes would go into `public/@scope/package/index.json` and tars
`tars/@scope/package/version.tar.gz`, which was then LINKED to `public/@scope/package/~/version.tar.gz`.
As we're maintaining the tars separately, they need to be COPIED into
`keybase://private/{username},ludd/@scope/package/~/version.tar.gz` which is then
read by the Keybase bot and copied into `keybase://private/ludd,kbpbot/@scope/package/~/version.tar.gz`
so that the package can be served from the global DNS-resolved address.

<table>
 <thead>
  <tr>
   <th>Argument</th> 
   <th>Short</th>
   <th>Description</th>
  </tr>
 </thead>
  <tr>
   <td>--init</td>
   <td>-i</td>
   <td>Initialise registry.</td>
  </tr>
  <tr>
   <td>--blank</td>
   <td>-b</td>
   <td>Creates indexes without extensions.</td>
  </tr>
  <tr>
   <td>--html</td>
   <td>-H</td>
   <td>Create HTML indexes instead of JSON.</td>
  </tr>
  <tr>
   <td>--conf</td>
   <td>-c</td>
   <td>The path to the config file. Default <code>index.js</code>.</td>
  </tr>
  <tr>
   <td>--meta</td>
   <td>-m</td>
   <td>Fill registry with meta information. Needs
    to be called each time after a new version
    of a package is added.</td>
  </tr>
  <tr>
   <td>--dest</td>
   <td>-o</td>
   <td>The path to the registry's public directory. Default <code>public</code>.</td>
  </tr>
  <tr>
   <td>--tars</td>
   <td>-t</td>
   <td>The path to the registry's directory for tars. Default <code>public</code>.</td>
  </tr>
  <tr>
   <td>--copy</td>
   <td></td>
   <td>Copy tars instead of linking them.</td>
  </tr>
  <tr>
   <td>--registry</td>
   <td>-r</td>
   <td>The URL of the registry to publish on, e.g.,
    <code>https://luddites.gitlab.io</code>. Equivalent to
    setting <code>url</code> in the config file.</td>
  </tr>
  <tr>
   <td>--help</td>
   <td>-h</td>
   <td>Print the help information and exit.</td>
  </tr>
  <tr>
   <td>--version</td>
   <td>-v</td>
   <td>Show the version's number and exit.</td>
  </tr>
</table>

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="/.documentary/section-breaks/3.svg?sanitize=true">
</a></p></div>

## Copyright & License

See `@artdeco/EULA` in [LICENSE](LICENSE). **Not** Open Source.
You'll be sued when using any of the source code without permission.

<table>
  <tr>
    <th width="100px">
      <a href="https://www.artd.eco">
        <img width="100" src="https://artdeco.gitlab.io/assets/logo-100.png" alt="Art Deco">
      </a>
    </th>
    <th>© <a href="https://www.artd.eco">Art Deco™</a>   2020</th>
  </tr>
</table>

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="/.documentary/section-breaks/-1.svg?sanitize=true">
</a></p></div>