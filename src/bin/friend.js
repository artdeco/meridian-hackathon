import { Keypair, Server } from 'stellar-sdk'
import rqt from 'rqt'
// create a completely new and unique pair of keys
// see more about KeyPair objects: https://stellar.github.io/js-stellar-sdk/Keypair.html
// const pair = Keypair.random()
const pair = Keypair.fromSecret('SBBJBWUJFDRVWJ36GBSNXJCPNJTH3WIQLRA6J3WWCGZI3VSQWD4MKNPD')

;(async () => {
  const resp = await rqt(
    `https://friendbot.stellar.org?addr=${encodeURIComponent(pair.publicKey())}`
  )
  const server = new Server("https://horizon-testnet.stellar.org")

  const account = await server.loadAccount(pair.publicKey())
  console.log("Balances for account: " + pair.publicKey())
  for (const balance of account.balances) {
    console.log("Type:", balance.asset_type, ", Balance:", balance.balance)
  }
})()
console.log(pair.secret())
console.log(pair.publicKey())