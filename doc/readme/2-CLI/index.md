## CLI

There are 2 CLI programs that are used.

First, `neoluddite` is a piece of software that builds registry indexes from the
local database that contains meta records about packages. This is outside
the scope of this hackathon and was developed as part of the _Luddites_ projects.

However, it was modified to adapt for the needs of the hackathon in the following ways:

1. The `blank` argument was added to enable to generate indexes without extension.
GitLab, for example, could serve `index.html` at `https://registry.gitlab.com/@scope/package/`
appropriately, but Keybase will serve a redirect request to `package/` which won't be
handled by `npm` and `yarn` clients appropriately. This means that we can simply create
`@scope/package` file with the JSON information about the versions that are then installed.
1. An additional `tars` option was added, to specify a distinct folder where the packed
packages are placed. Previously, they would go in the public folder, but because the
the indexes are now files and not `index.html` files inside directories, TARs need to go
into a separate folder, as one cannot write to `@scope/package`.
1. Finally, in the previous version, the registry was maintained as a single repository
where indexes would go into `public/@scope/package/index.json` and tars
`tars/@scope/package/version.tar.gz`, which was then LINKED to `public/@scope/package/~/version.tar.gz`.
As we're maintaining the tars separately, they need to be COPIED into
`keybase://private/{username},ludd/@scope/package/~/version.tar.gz` which is then
read by the Keybase bot and copied into `keybase://private/ludd,kbpbot/@scope/package/~/version.tar.gz`
so that the package can be served from the global DNS-resolved address.

<argufy>../neoluddite/types/arguments/main.xml</argufy>

<!-- <fork>src/bin -h</fork> -->

%~%