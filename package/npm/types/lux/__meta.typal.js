/**
 * Checks arguments' types.
 * @param  {...!Array} args
 */
function __checkArgs(...args) {
  args.forEach((ar) => {
    const [arg, name, expectedType, optional = false, nullable = false] = ar
    const A = `Argument "${name}"`
    if (arg === undefined) {
      if (!optional) throw new TypeError(`${A} is not optional. Received undefined.`)
      return
    }
    if (arg === null) {
      if (!nullable) throw new TypeError(`${A} is not nullable. Received null.`)
      return
    }
    const actual = typeof arg
    if (actual != expectedType) {
      throw new TypeError(`${A} must be a ${expectedType}. Received ${actual}.`)
    }
  })
}

/** @type {_protypes.Options} */
const SUBTYPE_OPTS = { setProtypesPrototype: true, mergeGettersSetters: true, overrideTarget: true, methodDecorators: [
  (method, propName, target) => { const p = target[propName] || {}; const { checkArgs } = p; if (checkArgs) return function (...args) {
    checkArgs(...args); return method.call(this, ...args)
  } },
] }

/** @suppress {nonStandardJsDocs} @typedef {import('protypes').Options} _protypes.Options */

module.exports.__checkArgs = __checkArgs
module.exports.SUBTYPE_OPTS = SUBTYPE_OPTS