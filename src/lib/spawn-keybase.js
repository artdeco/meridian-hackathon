import { join } from 'path'
import spawn from 'spawncommand'

const KEYBASE = 'keybase'

/**
 * Execute Keybase binary methods without API.
 * @param {import('keybase-bot/lib/utils/keybaseStatus').BotInfo} info
 * @param  {...string} args
 */
export const spawnKeybase = async (info, ...args) => {
  const [arg] = args
  if (arg == 'fs' && info.homeDir) {
    console.log('keybase fs doesn\'t work with homedir. Login onto'
      + ' Keybase service with the bot account in parallel to the bot session.')
    info = { ...info, homeDir: null }
  }
  const { homeDir } = info
  const keybase = homeDir ? join(homeDir, KEYBASE) : KEYBASE
  const Args = homeDir ? ['--home', homeDir, ...args] : args
  const p = spawn(keybase, Args)
  console.log(p.spawnCommand)
  const { code, stdout, stderr } = await p.promise
  if (code) throw new Error(stderr)
  return { stdout, stderr }
}
