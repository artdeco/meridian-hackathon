/* {{ license.assertion }} */

const { c } = require('../../compile/stdlib');
const { reduceUsage } = require('../../compile/stdlib');
const { readFileSync, writeFileSync } = require('fs');
const { usually } = require('../../compile/stdlib');
const meridianHackathon = require('../');
require('../../types/externs');
const Init = require('./commands/init');
const { argsConfig, _help, _init, _input, _output, _version } = require('./get-args');

if (_help) {
  const usage = usually({
    description: 'The Implementation Of Package.Market For The Meridian Hackathon Submission.',
    example: 'meridian-hackathon example.txt -o out.txt',
    line: 'meridian-hackathon input [-o output] [-ihv]',
    usage: reduceUsage(argsConfig),
  })
  console.log(usage)
  process.exit(0)
} else if (_version) {
  console.log(require('../../package.json').version)
  process.exit(0)
}

(async () => {
  try {
    if (_init) return await Init()
    if (!_input) throw new Error('Please pass an input file.')
    const content = /** @type {string} */ (readFileSync(_input, 'utf8'))
    const output = await meridianHackathon({
      shouldRun: true,
      text: content,
    })
    if (_output == '-') console.log(output)
    else writeFileSync(_output, output)
    console.error('File %s successfully processed.', c(_input, 'yellow'))
  } catch (err) {
    if (process.env['DEBUG']) console.error(err.stack)
    else console.log(err.message)
  }
})()