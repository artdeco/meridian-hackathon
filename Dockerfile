FROM keybaseio/client:nightly-node

WORKDIR /app
COPY package.json ./
COPY yarn.lock ./
COPY .npmrc .npmrc

# Use the following command to set up dokku
# dokku docker-options:add ml build '--build-arg ARTDECO_TOKEN=`dokku config:get ludd LUDDITES_TOKEN`'

ARG ARTDECO_TOKEN

RUN echo $ARTDECO_TOKEN
RUN yarn
RUN rm -f .npmrc

COPY . .

ENV NODE_ENV production

CMD [ "node", "node_modules/.bin/alanode", "src/bin/bot" ]