/* {{ license.assertion }} */

require('../types/externs');
const meridianHackathon = require('./'); const { MeridianHackathon } = meridianHackathon;

module.exports = {
  '_meridianHackathon': meridianHackathon,
  '_MeridianHackathon': MeridianHackathon,
}