/* start example */
import { MeridianHackathon } from '../../'

/* end example */
;(async () => {
  /* start example */
  let meridian-hackathon = new MeridianHackathon()
  let res = await meridian-hackathon.run({
    shouldRun: true,
    text: 'hello-world',
  })
  console.log(res) // @luddites/meridian-hackathon called with hello-world
  /* end example */
})()