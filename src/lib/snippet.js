import { Keypair, Server, Networks, Operation, TransactionBuilder, BASE_FEE, Asset, Claimant } from 'stellar-sdk'

const server = new Server('https://horizon-testnet.stellar.org')

// https://runkit.com/tyvdh/5f96e17c7640e1001abb56d0
// https://runkit.com/tyvdh/5f89de9540b734001aac057f

/**
 * Creates a claimable balance. If the public key isn't passed, creates a new account
 * also by sponsoring reserves. Gets users set up quickly.
 * @param {string} amount The amount to create a claim for.
 * @param {string} secret Stellar secret key to initialise the account with.
 * @param {string} [sponsoredPublicKey] The key which will claim the balance.
 * @param {number|string} [sponsoringCost] If sponsoring a new account, how much to provide for the user.
 * @returns {Keypair} A keypair for the new account, or null if the claimable balance was created without a new account.
 */
export const createClaimableBalance = async (amount, secret, sponsoredPublicKey, sponsoringCost = 2) => {
  if (!amount) throw new Error('Please specify the amount for claiming.')

  const sponsorKeypair = Keypair.fromSecret(secret)

  /** @type {Keypair} */
  let sponsoredKeypair = null, needsSponsoring = false
  if (!sponsoredPublicKey) { // create a new keypair if needed
    sponsoredKeypair = Keypair.random()
    sponsoredPublicKey = sponsoredKeypair.publicKey()
    needsSponsoring = true
  }

  const sponsorPublicKey = sponsorKeypair.publicKey()

  const account = await server.loadAccount(sponsorPublicKey)
  for (const balance of account.balances) {
    console.log("Type:", balance.asset_type, ", Balance:", balance.balance)
  }

  const balance = needsSponsoring ? amount - sponsoringCost : amount
  let transaction = new TransactionBuilder(account, {
    fee: BASE_FEE,
    networkPassphrase: Networks.TESTNET,
  })
    .addOperation(Operation.createClaimableBalance({
      asset: Asset.native(),
      amount: `${balance}`,
      claimants: [
        new Claimant(sponsoredPublicKey),
      ],
    }))
    .setTimeout(0)
    .build()

  transaction.sign(sponsorKeypair)
  await server.submitTransaction(transaction)

  if (needsSponsoring) {
    await sponsorAccount(sponsorKeypair, sponsoredKeypair, sponsoringCost)
  }

  return sponsoredKeypair
}

const sponsorAccount = async (sponsorKeypair, sponsoredKeypair, startingBalance = 0) => {
  const sponsorPublicKey = sponsorKeypair.publicKey()
  const sponsoredPublicKey = sponsoredKeypair.publicKey()

  const account = await server.loadAccount(sponsorPublicKey)
  const transaction = new TransactionBuilder(account, {
    fee: BASE_FEE,
    networkPassphrase: Networks.TESTNET,
  })
    .addOperation(Operation.beginSponsoringFutureReserves({
      sponsoredId: sponsoredPublicKey,
    }))
    .addOperation(Operation.createAccount({
      destination: sponsoredPublicKey,
      startingBalance: `${startingBalance}`,
    }))
    .addOperation(Operation.endSponsoringFutureReserves({
      source: sponsoredPublicKey,
    }))
    .setTimeout(0)
    .build()

  transaction.sign(sponsorKeypair, sponsoredKeypair)
  await server.submitTransaction(transaction)
}

/**
 * Claim all balances for the account.
 * @param {string} secret The Stellar secret key.
 */
export const claim = async (secret) => {
  const sponsoredKeypair = Keypair.fromSecret(secret)

  const publicKey = sponsoredKeypair.publicKey()
  let account = await server.loadAccount(publicKey)

  const { records } = await server
    .claimableBalances()
    .claimant(account.id)
    .call()

  if (!records.length) return

  const transaction = new TransactionBuilder(account, {
    fee: BASE_FEE,
    networkPassphrase: Networks.TESTNET,
  })
    .addOperation(Operation.claimClaimableBalance({
      balanceId: records[0].id,
    }))
    .setTimeout(0)
    .build()

  transaction.sign(sponsoredKeypair)

  const resp = await server.submitTransaction(transaction)
}

/**
 * Pay for the package using client's secret key.
 * @param {string} secret The client's Stellar secret key.
 * @param {string} receiverPublicKey The Ludds public key.
 * @param {string|number} amount How much a package costs.
 */
export const transfer = async (secret, receiverPublicKey, amount) => {
  if (!amount) throw new Error('Missing price for the package.')

  const sourceKeypair = Keypair.fromSecret(secret)

  const publicKey = sourceKeypair.publicKey()
  let account = await server.loadAccount(publicKey)

  const transaction = new TransactionBuilder(account, {
    fee: BASE_FEE,
    networkPassphrase: Networks.TESTNET,
  })
    .addOperation(Operation.payment({
      destination: receiverPublicKey,
      asset: Asset.native(),
      amount: `${amount}`,
    }))
    .setTimeout(0)
    .build()

  transaction.sign(sourceKeypair)

  await server.submitTransaction(transaction)
}

export async function getBalances(publicKey) {
  const account = await server.loadAccount(publicKey)
  const s = '```\n' + account.balances.map(({ balance, asset_type }) => {
    return `${asset_type} :: ${balance}`
  }).join('\n') + '\n```'
  return s
}

// (async () => {
//   try {
//

//     const pair = await createClaimableBalance(100, sponsorKeypair)
//     if (pair) {
//       await claim(pair)
//     }
//   } catch (err) {
//     const { response: { data } } = err
//     console.error(data)
//   }
// })()