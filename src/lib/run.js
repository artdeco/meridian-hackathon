import { c } from '@artdeco/erte'

/** @type {_meridianHackathon.meridianHackathon} */
export default async function meridianHackathon(config = {}) {
  const {
    shouldRun = true,
    text = '',
  } = config
  if (!shouldRun) return ''
  console.log('@luddites/meridian-hackathon called with %s', c(text, 'yellow'))
  return text
}

/** @typedef {import('../../').meridianHackathon} _meridianHackathon.meridianHackathon */
