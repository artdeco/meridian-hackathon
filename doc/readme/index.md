<public gitlab dir="doc/pub" /><div align="center"><p align="center">

# @luddites/meridian-hackathon

%NPM: @luddites/meridian-hackathon%
<pipeline-badge/>
</p></div>

`@luddites/meridian-hackathon` is: The Implementation Of Package.Market For The Meridian Hackathon Submission.

```sh
yarn add @luddites/meridian-hackathon
npm i @luddites/meridian-hackathon
```

## Table Of Contents

%TOC%

%~%

- Ludds.io: Package Market. Encouraging Paid Software.
- All works of art deserve to be paid and software is no different. Let's rethink current approach to package distribution and start charging people for our hard work on the new package.market.

## Submission

This is the text for the Hackathon Page.

### Inspiration

I really tried to identify myself with the world of Open Source, however couldn't in the end and gave up. Having made over 200 npm packages, including a novel testing and documentation frameworks, I've discovered that people are simply not interested in my work. It seems that once cool, open-minded movement of techies has become a biased, entitled mass that thinks "software = free" and that's everything there is to it. After finally realising that nobody is going to support my work, I decided to do what's usually done to make a living: sell my software.

In Democracy, people are allowed to hold various informed opinions. I'd like to give people the choice of selling their packages. There are many, who wouldn't mind putting a price tag on a piece of code, which they thoroughly tested, documented and perfected during sleepless hours, but the industry doesn't give them a chance simply because there's no platform. When estimated, many would find themselves surprised that software that they upload online for free, costs at least a few thousand dollars, and can bring recurring revenue.

Open Source isn't really fair to everyone. Once a maintainer has captured the market with his free tool, he/she will dominate it simply because the tool becomes the standard, and nobody else receives a chance to showcase their great packages and build up the market share, too. The "free" model evolves into predatory pricing strategy, while it is the customer who's suffering when they cannot receive adequate customer service because the maintainers use excuses like "we're volunteers" even though they're not providing fair support plans either.

I don't want donations. I don't want community. I want to build, market and sell. I want to create a brand and earn reputation as a software vendor whom individual customers as well as companies love dealing with, because they know that I care. My view is completely antagonistic to the FOSS paradigm, and I believe that although the user might feel like she has the right to study and improve the program, the software publisher also has the right to keep the source closed as his intellectual property. That's why I'm passionate about building a package market.

### What it does

The entry point to the app is two-fold.

First, package makers upload their work to the new registry (ludds.io). Using Keybase as the cryptographically sound client behind the package storage, the registry allows package makers to:

- upload the indexes of packages (such as @scope/package-name) to their public folder at username.keybase.pub/packages so that other people are able to access those indexes and be sure that they were created by the publisher, and not tampered with. The download links to actual archives with distributed material are signed using npm's and yarn's shasum algorithm to ensure authenticity.
- upload the actual packages to a private folder at `keybase://private/username,ludd` which is shared with the bot called ludd. Once uploaded, the bot moves the tar.gz archives onto its own private folder shared with kbpbot that will serve them from Keybase pages instance. The packages.ludds.io domain is configured to serve the files according to Keybase's documentation.

The security of packages is ensured by the fact that the index file is downloadable from the user's pub directory, visually identifiable as USERNAME.keybase.pub. As shasums are present, even though the domain that hosts the archives isn't transparent, the consumers are sure that the package was published by the real owner.

Secondly, those who wish to consume packages, need talk to the `ludd` chat bot, who'll process their requests. Users can search for packages using keywords and natural language (not implemented in hackathon) and once they found the suitable package, complete the purchase.

### How I built it

### Challenges I ran into

### Accomplishments that I'm proud of

### What I learned

### What's next for Łudds: Package Currency. Encouraging Paid Software Business.

OK

%~%