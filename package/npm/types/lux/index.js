const { SUBTYPE_OPTS } = require('./__meta.typal');
const { $name, is, subtype } = require('@protypes/protypes');

/**
 * A representation of a package.
 * @abstract @implements {_meridianHackathon.MeridianHackathon}
 */
class AbstractMeridianHackathon {
  /**
   * Constructor method.
   */
  constructor() {
    /**
     * An example property. Default `ok`.
     * @type {string}
     */
    this.example = 'ok'
  }
  /**
   * Executes main method via the class.
   * @param {!_meridianHackathon.MeridianHackathonConfig} conf The config to run the method against.
   * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
   * - `[text]` _string?_ A text to return.
   * @return {!Promise<string>} The result of transforms.
   * @example
   * ```js
   * import { MeridianHackathon } from '@luddites/meridian-hackathon'
   *
   * let meridian-hackathon = new MeridianHackathon()
   * let res = await meridian-hackathon.run({
   *   shouldRun: true,
   *   text: 'hello-world',
   * })
   * console.log(res) // @luddites/meridian-hackathon called with hello-world
   * ```
   */
  async run(conf) { }
  /**
   * Sets the implementation on the interface.
   * @param {...!(typeof AbstractMeridianHackathon|AbstractMeridianHackathon)} Implementations
   * @suppress {checkPrototypalTypes}
   */
  static __implement(...Implementations) {
    return subtype(AbstractMeridianHackathon, SUBTYPE_OPTS, ...Implementations)
  }
  get [$name]() { return 'MeridianHackathon' }
  /**
   * Checks if the instance implements the constructor by its name.
   * @param {string} className
   */
  __$implements(className) {
    return is(this, className)
  }
}

/** @typedef {import('../..').Config} _meridianHackathon.Config */

module.exports = AbstractMeridianHackathon