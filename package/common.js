export const website = {
  protocol: 'https',
  host: 'artdeco.software',
  pathname: `/meridian-hackathon/`,
}