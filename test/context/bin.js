import frameOfMind from 'frame-of-mind'
import { c } from '@artdeco/erte'
import { EOL } from 'os'
import { join, resolve } from 'path'

let TESTING_FREE = false, pckg, BIN

switch (process.env.ALAMODE_ENV) {
case 'test-compile': {
  pckg = 'compile/paid'
  break
} case 'test-build': {
  pckg = 'build/free'
  break
} case 'test-npm': {
  TESTING_FREE = '[throws] '
  pckg = 'package/npm' // fuck npm
  break
} case 'test-luddites': {
  pckg = 'package/luddites'
  break
}}

if (pckg) {
  const { bin: {
    meridian-hackathon,
  } } = require(resolve(pckg, 'package.json'))
  BIN = join(pckg, meridian-hackathon)
} else {
  pckg = 'source'
  BIN = 'src/bin'
}

console.log(
  frameOfMind([
    c(`Testing ${pckg} bin from`, 'blue', { bold: true }),
    c(BIN, 'blue', { reverse: true }),
  ].join(EOL)))

BIN = join(process.cwd(), BIN)
export { BIN, TESTING_FREE }