/*!
 * meridianHackathon: The Implementation Of Package.Market For The Meridian Hackathon Submission.
 *
 * Copyright (C) 2020  Art Deco Code Limited
 *
 * This program is NOT free software: you cannot redistribute, modify,
 * reverse-engineer and/or embed into other programs under the terms
 * of the Art Deco EULA which comes together with this distribution, and
 * is available at artdeco.legal. You're allowed to use this program as
 * a DEVTOOL with the license key issued per user.
 */

export default false