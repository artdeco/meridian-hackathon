/**
 * @fileoverview
 * @externs
 */

/* typal types/index.xml externs */
/** @const */
var _meridianHackathon = {}
/**
 * Options for the program.
 * @record
 */
_meridianHackathon.Config = function() {}
/**
 * A boolean option. Default `true`.
 * @type {boolean|undefined}
 */
_meridianHackathon.Config.prototype.shouldRun
/**
 * A text to return.
 * @type {string|undefined}
 */
_meridianHackathon.Config.prototype.text
/**
 * A representation of a package.
 * Constructor method.
 * @interface
 */
_meridianHackathon.MeridianHackathon = function() {}
/**
 * An example property. Default `ok`.
 * @type {string}
 */
_meridianHackathon.MeridianHackathon.prototype.example
/**
 * Executes the processing of data.
 * @param {!_meridianHackathon.Config} conf The config to run against.
 * @return {!Promise<string>} The result of trasforms.
 * @example
 * ```js
 * import { MeridianHackathon } from '@luddites/meridian-hackathon'
 *
 * let meridian-hackathon = new MeridianHackathon()
 * let res = await meridian-hackathon.run({
 *   shouldRun: true,
 *   text: 'hello-world',
 * })
 * console.log(res) // @luddites/meridian-hackathon called with hello-world
 * ```
 */
_meridianHackathon.MeridianHackathon.prototype.run = function(conf) {}

/* typal types/api.xml externs */
/**
 * The Implementation Of Package.Market For The Meridian Hackathon Submission.
 * @typedef {function(!_meridianHackathon.Config): !Promise<string>}
 */
_meridianHackathon.meridianHackathon
