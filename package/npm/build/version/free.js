/*!
 * meridianHackathon: The Implementation Of Package.Market For The Meridian Hackathon Submission.
 *
 * Copyright (C) 2020  Art Deco Code Limited
 *
 * This program is NOT free software: you cannot redistribute, modify,
 * reverse-engineer and/or embed into other programs under the terms
 * of the Art Deco EULA which comes together with this distribution, and
 * is available at artdeco.legal. You're allowed to use this program as
 * a DEVTOOL with the license key issued per user.
 *
 * When using a public free version of the package, you're automatically
 * granted an implicit license key upon download. This key works with
 * the FREE version only, and does not extend to the full paid version
 * of this package. All terms and conditions of Art Deco EULA apply.
 */

module.exports=true