/* {{ license.assertion }} */

import { c } from '@artdeco/erte'
import { reduceUsage } from 'argufy'
import { readFileSync, writeFileSync } from 'fs'
import usually from 'usually'
import meridianHackathon from '../'
import '../../types/externs'
import Init from './commands/init'
import { argsConfig, _help, _init, _input, _output, _version } from './get-args'

if (_help) {
  const usage = usually({
    description: 'The Implementation Of Package.Market For The Meridian Hackathon Submission.',
    example: 'meridian-hackathon example.txt -o out.txt',
    line: 'meridian-hackathon input [-o output] [-ihv]',
    usage: reduceUsage(argsConfig),
  })
  console.log(usage)
  process.exit(0)
} else if (_version) {
  console.log(require('../../package.json').version)
  process.exit(0)
}

(async () => {
  try {
    if (_init) return await Init()
    if (!_input) throw new Error('Please pass an input file.')
    const content = /** @type {string} */ (readFileSync(_input, 'utf8'))
    const output = await meridianHackathon({
      shouldRun: true,
      text: content,
    })
    if (_output == '-') console.log(output)
    else writeFileSync(_output, output)
    console.error('File %s successfully processed.', c(_input, 'yellow'))
  } catch (err) {
    if (process.env['DEBUG']) console.error(err.stack)
    else console.log(err.message)
  }
})()