import { equal } from '@zoroaster/assert'
import Context from '../context'
import MeridianHackathon from '../../src/lib/MeridianHackathon'

/** @type {TestSuite} */
const T = {
  context: Context,
  async'gets a link to the fixture'({ fixture, readFile }) {
    const m = new MeridianHackathon()
    await m.run()
  },
}

/** @typedef {import('../context').TestSuite} TestSuite */

export default T