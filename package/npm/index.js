const { _meridianHackathon, _MeridianHackathon } = require('./compile/meridian-hackathon')

/**
 * The Implementation Of Package.Market For The Meridian Hackathon Submission.
 * @param {!Config} config Options for the program.
 * @param {boolean} [config.shouldRun=true] A boolean option. Default `true`.
 * @param {string} [config.text] A text to return.
 * @return {!Promise<string>} The result of processing.
 */
function $meridianHackathon(config) {
  return _meridianHackathon(config)
}

/**
 * A representation of a package.
 */
class MeridianHackathon extends _MeridianHackathon {
  /**
   * Constructor method.
   */
  constructor() {
    super()
    /**
     * An example property. Default `ok`.
     * @type {string}
     */
    this.example
  }
  /**
   * Executes the processing of data.
   * @param {!Config} conf Options for the program.
   * @param {boolean} [conf.shouldRun=true] A boolean option. Default `true`.
   * @param {string} [conf.text] A text to return.
   * @return {!Promise<string>} The result of trasforms.
   * @example
   * ```js
   * import { MeridianHackathon } from '@luddites/meridian-hackathon'
   *
   * let meridian-hackathon = new MeridianHackathon()
   * let res = await meridian-hackathon.run({
   *   shouldRun: true,
   *   text: 'hello-world',
   * })
   * console.log(res) // @luddites/meridian-hackathon called with hello-world
   * ```
   */
  async run(conf) {
    return super.run(conf)
  }
}

module.exports = exports = $meridianHackathon
module.exports.MeridianHackathon = MeridianHackathon

/** @typedef {$meridianHackathon} meridianHackathon */

/* typal types/index.xml */
/**
 * @typedef {Object} Config `＠record` Options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */
